package com.imi.reporting;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.imi.reporting.mongo.model.User;
import com.imi.reporting.mongo.repo.UserRepository;
import com.imi.reporting.mysql.model.Student;
import com.imi.reporting.mysql.repo.StudentRepository;

@SpringBootTest
class BaseprojectApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private StudentRepository studentRepo;
	
	
	@Test
	void check() {
		System.out.println("Runn successfully");
	}
	
	@Test
	public void createUser() {
		User user = new User();
		user.setName("Nandan");
		user.setAge(26);
		
		userRepo.save(user);
	     
		System.out.println(user);
	}
	
	
	@Test
	public void createStudent() {
		Student student = new Student();
		student.setFirstName("Shreya");
		student.setLastName("Nitesh");
		student.setCity("dumka");
		student.setScore(98);
		
		studentRepo.save(student);
		System.out.println(student);
	}
	
	
	@Test
	public void printData() {
		List<User> userList = userRepo.findAll();
		List<Student> studentList = (List<Student>) studentRepo.findAll();
		
		System.out.println(userList);
		System.out.println(studentList);
	}

}
