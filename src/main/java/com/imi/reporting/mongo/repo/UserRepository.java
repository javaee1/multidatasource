package com.imi.reporting.mongo.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.imi.reporting.mongo.model.User;


public interface UserRepository extends MongoRepository<User,String> {

}
