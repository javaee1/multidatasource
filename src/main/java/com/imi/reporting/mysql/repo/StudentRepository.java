package com.imi.reporting.mysql.repo;

import org.springframework.data.repository.CrudRepository;

import com.imi.reporting.mysql.model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
